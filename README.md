# Strongswan on Docker

Base docker image to run a Strongswan IPsec server.

## Usage

Run the following to start the container:

```
docker run -d --name cb --privileged --net=host -p 500:500/udp -p 4500:4500/udp -p 1701:1701/udp   qwzar/cloudvpn
```


## Volume / Configuration files

* /etc/ppp/l2tp-secrets
* /etc/ipsec.secrets
* /etc/ipsec.conf
* /etc/strongswan.conf
* /etc/xl2tpd.conf




ip address add 172.28.2.10/24 dev eth0

   11  sudo docker run -d --name cb --privileged --net=host -p 500:500/udp -p 4500:4500/udp -p 1701:1701/udp   qwzar/cloudvpn
   12  sudo docker ps
   13  sudo docker exect -it cb bash
   14  sudo docker exec -it cb bash
   15  exit
   16  sudo nano .ssh/authorized_keys
   17  ssh-keygen -t rsa
   18  sudo nano .ssh/authorized_keys
   19  sudo docker exec -it cb bash
   20  sudo docker ps
   21  sudo docker exec -it cb bash
   24  sudo sysctl -w net.ipv4.icmp_echo_ignore_all=0
   25  ping 172.28.2.10
   26  sudo docker exec -it cb bash
   27  sysctl -p
   28  sudo sysctl -p
   29* sudo echo 0 >/proc/sys/net/ipv4/icmp_echo_ignore_al
   30  sudo echo 0 >sudo /proc/sys/net/ipv4/icmp_echo_ignore_all
   31  sudo nano /etc/sysctl.conf
   32  cat /proc/sys/net/ipv4/icmp_echo_ignore_all
   33  sudo docker exec -it cb bash
   34  iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
   35  iptables -A OUTPUT -p icmp --icmp-type echo-reply -j ACCEPT
   36  sudo iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
   37  sudo iptables -A OUTPUT -p icmp --icmp-type echo-reply -j ACCEPT
   38  sudo docker exec -it cb bash